---
layout: handbook-page-toc
title:  Gitlab Laptop Delivery Metrics
---
## On this page
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

## Gitlab Laptop Delivery Metrics

### Purpose

We're designing this page to publicly and transparently show our metrics around laptop shipping, delivery, and procurement. This page is in its early stage and we will iterate and add more content as we continue to advance our metrics. 

### Table Breakdown

The **Regions** section is detailing what region and what metric we are tracking, within we are reporting total laptops delivered per region and percentage of on time delivery for the related month. The acronym ROW stands for **Rest of the World**, these are region that we hire in but do not have a registered vendor or shipping entity. Please note these numbers do not reflect new hires or team members that opt to self-procure their own laptop. 


 
| Regions                         | January | February | March | April | May | June | July |
| -------------                   |:--------|:---------|:------|:------|:----|:-----|:-----|
| EMEA Laptops delivered          |6        |5         |5      |5      |10   |4     |11    |
| EMEA % on time                  |83.40%   |100%      |80%    |80%    |100% |100%  |100%  |
| North America laptops delivered |7        |13        |13     |11     |16   |15    |22    |
| North America % on time         |100%     |84.70%    |100%   |81.9   |82%  |93.4% |92%   |
| APAC Laptops delivered          |5        |2         |3      |1      |4    |6     |6     |
| APAC % on time                  |80%      |100%      |67%    |100%   |100% |66.70%|100%  |
| LATAM Laptops delivered         |0        |0         |0      |0      |0    |0     |0     |
| LATAM % on time                 |N/A      |N/A       |N/A    |N/A    |N/A  |N/A   |N/A   |
| ROW Laptops delivered           |0        |0         |0      |0      |0    |0     |0     |
| ROW % on time                   |N/A      |N/A       |N/A    |N/A    |N/A  |N/A   |N/A   |
| Total laptops delivered         |18       |20        |21     |19     |30   |25    |39    |
| % Laptops delivered on time     |88.90%   |90%       |91%    |84.3   |90%  |88%   |95%   |

